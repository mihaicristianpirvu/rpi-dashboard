package services

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"rpi-dashboard/internal/utils"
	"strconv"
	"strings"
)

type Service struct {
	Name string
	Command string
	DirPath string
	FilePath string
	Port string
}

func makeService(name string, command string, path string, port string) Service {
	var pathSplit = strings.Split(path, "/")
	var pathDir = strings.Join(pathSplit[:len(pathSplit) - 1], "/")
	var pathFile = pathSplit[len(pathSplit) - 1]
	return Service{Name: name, Command: command, DirPath: pathDir, FilePath: pathFile, Port: port}
}

func MakeServices(services map[string]interface{}) map[string]Service {
	var res = make(map[string]Service)
	for k, v := range services {
		var items = strings.Split(v.(string), " ")
		if len(items) != 3 { log.Fatalf("Format must be: 'command /path/to/script.sh port', got " + v.(string)) }
		res[k] = makeService(k, items[0], items[1], items[2])
	}
	return res
}

func (s Service) String() string {
	return fmt.Sprintf("Command: %s, DirPath: %s, FilePath: %s, Port: %s", s.Command, s.DirPath, s.FilePath, s.Port)
}

func (s Service) CheckService() (string, error) {
	netstatCommand := "netstat -tlpn 2>/dev/null | awk '{print $4, $7}' | column -t | tail -n +3 | grep -v '::' " +
					  "| tr -s \"  \" \" \""
	out, err := exec.Command("bash", "-c", netstatCommand).Output()
	if err != nil { log.Fatalf(err.Error()) }

	processesList := strings.Split(string(out), "\n")
	for i, line := range processesList {
		if i == len(processesList) - 1 { break }
		lineSplit := strings.Split(line, " ")
		if len(lineSplit) != 2 { log.Fatalf("Unexpected output from netstat: %s", line) }
		ipPort, pidApp := lineSplit[0], lineSplit[1]
		_, port := strings.Split(ipPort, ":")[0], strings.Split(ipPort, ":")[1]
		if port != s.Port { continue }
		pid := strings.Split(pidApp, "/")[0]
		if (pid != "-") && (pid != "") { return pid, nil }
	}
	return "", fmt.Errorf("Service '%s' not found", s)
}

func SetupNginx(services map[string]Service, rootDir string) string {
	configFile := `
	server {
		listen 80 default_server;
		listen [::]:80 default_server;
		root ` + rootDir + `/html;

		index index.html index.htm index.nginx-debian.html;

		server_name _;

		location / {
			try_files $uri $uri/ =404;
		}
`

	serviceNames := utils.SortedMapKeys(services)
	for _, serviceName := range serviceNames {
		service := services[serviceName]
		serviceConfig := `
		location /` + serviceName + `/ {
			proxy_pass http://localhost:` + service.Port  + `/;
		}

		location /` + serviceName + ` {
			return 301 /` + serviceName + `/;
		}
`
		configFile += serviceConfig
	}

	configFile += `
	}
`
	return configFile
}

func ServicesToHtml(services map[string]Service, baseHtml string) string {
	data, err := os.ReadFile(baseHtml)
	if err != nil {log.Fatalf("error reading file %s: %s", baseHtml, err)}
	lines := strings.Split(string(data), "\n")
	newHtml := []string{}
	ix := 0
	for i, line := range lines {
		if strings.Compare(line, `<!-- HERE services will be added as a new <a> with class="item"-->`) == 0 {
			ix = i
			break
		}
		newHtml = append(newHtml, line)
	}

	serviceNames := utils.SortedMapKeys(services)
	for i, serviceName := range serviceNames {
		thisRow := `<a class="item" href="` + serviceName + `">` + utils.Capitalize(serviceName) + `</a>`
		if i % 3 == 2 {
			thisRow += "<br/>"
		}
		newHtml = append(newHtml, thisRow)
	}

	newHtml = append(newHtml, lines[ix:]...)
	return strings.Join(newHtml, "\n")
}

func gatherPids(services map[string]Service) (map[string]string, map[string]error) {
	errs := map[string]error{}
	pids := map[string]string{}
	for _, service := range services {
		errs[service.Name] = nil
		pids[service.Name] = ""
		lsofCommand := "lsof -i:" + service.Port + " -t"
		out, err := exec.Command("bash", "-c", lsofCommand).Output()
		if err != nil {
			errs[service.Name] = err
			continue
		}
		pids[service.Name] = strings.TrimSpace(string(out))
		fmt.Printf(service.Name + ", PID: " + pids[service.Name] + "\n")
	}
	return pids, errs
}

func KillOneService(services map[string]Service, serviceToKill string) error {
	pids, errs := gatherPids(services)
	if errs[serviceToKill] != nil {
		return errs[serviceToKill]
	}
	pid, ok := pids[serviceToKill]
	if !ok {
		return fmt.Errorf("%s not found in returned pids %v", serviceToKill, pids)
	}
	ipid, err := strconv.Atoi(pid)
	if err != nil {
		return err
	}
	proc, err := os.FindProcess(ipid)
	if err != nil {
		return err
	}
	err = proc.Kill()
	return err
}

func KillAllServices(services map[string]Service) map[string]error {
	pids, errs := gatherPids(services)
	for serviceName, pid := range pids {
		ipid, err := strconv.Atoi(pid)
		if err != nil { continue } // failed above
		proc, err := os.FindProcess(ipid)
		if err != nil {
			errs[serviceName] = err
			continue
		}
		err = proc.Kill()
		errs[serviceName] = err
	}
	return errs
}

func StartOneService(service Service) (int, error) {
	res, err := service.CheckService()
	if err == nil {
		return -1, fmt.Errorf("%s already started at port: %s", service.Name, res)
	}
	cwd, err := os.Getwd()
	if err != nil {log.Fatalf(err.Error())}

	fmt.Printf("Starting %s at dirpath: %s\n", service.Name, service.DirPath)
	err = os.Chdir(service.DirPath)
	if err != nil {
		return -1, err
	}

	cmd := exec.Command(service.Command, service.FilePath, service.Port)
	err = os.MkdirAll(fmt.Sprintf("%s/logs/%s", utils.GetProjectRoot(), service.Name), 0755)
	if err != nil {
		os.Chdir(cwd)
		return -1, err
	}
	file, err := os.Create(fmt.Sprintf("%s/logs/%s/log.txt", utils.GetProjectRoot(), service.Name))
	if err != nil {
		os.Chdir(cwd)
		return -1, err
	}

	cmd.Stderr = file
	err = cmd.Start()
	if err != nil {
		os.Chdir(cwd)
		return -1, err
	}
	os.Chdir(cwd)
	return cmd.Process.Pid, nil
}

func StartAllServices(services map[string]Service) (map[string]error, map[string]int) {
	errs := map[string]error{}
	pids := map[string]int{}
	for _, service := range services {
		pid, err := StartOneService(service)
		errs[service.Name] = err
		pids[service.Name] = pid
	}
	return errs, pids
}
