package services

import (
	"fmt"
	"log"
	"os"
	"strings"
	"testing"

	"gopkg.in/yaml.v3"
	"rpi-dashboard/internal/utils"
)

func TestMakeServices(t *testing.T) {
	data := map[string]interface{}{"service1": "command /path/to/script.sh 8080"}
	result := MakeServices(data)
	if len(result) != 1 {t.Fatalf("Expected 1 service, got %d", len(result))}
	if result["service1"].Command != "command" {t.Fatalf("Expected command, got %s", result["service1"].Command)}
	if result["service1"].DirPath != "/path/to" {t.Fatalf("Expected /path/to, got %s", result["service1"].DirPath)}
	if result["service1"].FilePath != "script.sh" {t.Fatalf("Expected script.sh, got %s", result["service1"].FilePath)}
	if result["service1"].Port != "8080" {t.Fatalf("Expected 8080, got %s", result["service1"].Port)}
}

func TestMakeServicesTestFile(t *testing.T) {
	data, err := os.ReadFile(utils.GetProjectRoot() + "/test_services/test_services.yaml")
	if err != nil {
		log.Fatalf("error reading file: %v", err)
	}

	var result map[string]interface{}
	if err := yaml.Unmarshal(data, &result); err != nil {
		log.Fatalf("error reading yaml: %v", err)
	}

	servicesMap := MakeServices(result["services"].(map[string]interface{}))
	fmt.Println(servicesMap)

	if len(servicesMap) != 2 {t.Fatalf("Expected 2 services, got %d", len(servicesMap))}
	if servicesMap["serv1"].Command != "bash" {t.Fatalf("Expected bash, got %s", servicesMap["serv1"].Command)}
	if servicesMap["serv1"].DirPath != "test_services" {t.Fatalf("Expected test_services, got %s",
																 servicesMap["serv1"].DirPath)}
	if servicesMap["serv1"].FilePath != "run.sh" {t.Fatalf("Expected run.sh, got %s", servicesMap["serv1"].FilePath)}
	if servicesMap["serv1"].Port != "6969" {t.Fatalf("Expected 6969, got %s", servicesMap["serv1"].Port)}

	if servicesMap["serv2"].Command != "bash" {t.Fatalf("Expected bash, got %s", servicesMap["serv2"].Command)}
	if servicesMap["serv2"].DirPath != "test_services" {t.Fatalf("Expected test_services, got %s",
															     servicesMap["serv2"].DirPath)}
	if servicesMap["serv2"].FilePath != "run.sh" {t.Fatalf("Expected run.sh, got %s", servicesMap["serv2"].FilePath)}
	if servicesMap["serv2"].Port != "6970" {t.Fatalf("Expected 6970, got %s", servicesMap["serv2"].Port)}
}

func TestCheckStatus(t *testing.T) {
	data, err := os.ReadFile(utils.GetProjectRoot() + "/test_services/test_services.yaml")
	if err != nil { log.Fatalf("error reading file: %v", err) }

	var result map[string]interface{}
	if err := yaml.Unmarshal(data, &result); err != nil { log.Fatalf("error reading yaml: %v", err) }

	servicesMap := MakeServices(result["services"].(map[string]interface{}))
	for _, v := range servicesMap {
		_, err := v.CheckService()
		if err == nil {t.Fatalf("All services must be down when running the tests")}
	}
}

func TestNginxSetup(t *testing.T) {
	data, err := os.ReadFile(utils.GetProjectRoot() + "/test_services/test_services.yaml")
	if err != nil { log.Fatalf("error reading file: %v", err) }

	var result map[string]interface{}
	if err := yaml.Unmarshal(data, &result); err != nil {t.Fatalf("error reading yaml: %v", err)}

	servicesMap := MakeServices(result["services"].(map[string]interface{}))
	nginxSetup := SetupNginx(servicesMap, "test_services")
	expectedSetup, err := os.ReadFile(utils.GetProjectRoot() + "/test_services/expected_default")
	if err != nil { log.Fatalf(err.Error()) }
	expectedStr := string(expectedSetup)
	if strings.Compare(expectedStr, nginxSetup) != 0 {t.Fatalf("Should be equal: " + nginxSetup + "\n\n" + expectedStr)}
}

func TestServicesToHtml(t *testing.T) {
	data, err := os.ReadFile(utils.GetProjectRoot() + "/test_services/test_services.yaml")
	if err != nil { log.Fatalf("error reading file: %v", err) }

	var result map[string]interface{}
	if err := yaml.Unmarshal(data, &result); err != nil {t.Fatalf("error reading yaml: %v", err)}

	servicesMap := MakeServices(result["services"].(map[string]interface{}))
	nginxSetup := ServicesToHtml(servicesMap, utils.GetProjectRoot() + "/html/base_index.html")
	expectedIndex, err := os.ReadFile(utils.GetProjectRoot() + "/test_services/expected_index.html")
	if err != nil { log.Fatalf(err.Error()) }
	expectedStr := string(expectedIndex)
	if strings.Compare(expectedStr, nginxSetup) != 0 {t.Fatalf("Should be equal: " + nginxSetup + "\nvs\n" + expectedStr)}
}
