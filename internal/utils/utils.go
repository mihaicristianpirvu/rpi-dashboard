package utils

import (
	"log"
	"os"
	"os/exec"
	"reflect"
	"sort"
	"strings"
)

func GetProjectRoot() string {
	executablePath, err := os.Executable()
	if err != nil {
		log.Fatalf("Error on executable path %s", err.Error())
	}
	if strings.HasPrefix(executablePath, "/tmp") {
		// running with go run /path/to/dashboard-manager.go
		cmdOut, err := exec.Command("git", "rev-parse", "--show-toplevel").Output()
		if err != nil {
			log.Fatalf("Error on getting the go-kit base path: %s - %s", err.Error(), string(cmdOut))
		}
		return strings.TrimSpace(string(cmdOut))
	} else {
		// running the built binary
		splitString := strings.Split(executablePath, "/")
		cmdOut := strings.Join(splitString[0:len(splitString)-2], "/")
		return strings.TrimSpace(string(cmdOut))
	}
}

func SortedMapKeys(m interface{}) (keyList []string) {
	keys := reflect.ValueOf(m).MapKeys()

	for _, key := range keys {
		keyList = append(keyList, key.Interface().(string))
	}
	sort.Strings(keyList)
	return
}

func Capitalize(s string) string {
	return strings.ToUpper(s[0:1]) + s[1:]
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
