///usr/bin/true; exec /usr/bin/env go run "$0" "$@"
package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"gopkg.in/yaml.v3"

	"rpi-dashboard/pkg/services"
	"rpi-dashboard/internal/utils"
)

func main() {
	validCmds := []string{"check_services", "setup_nginx", "services_to_html", "stop_services", "start_services",
						  "stop_service", "start_service"}
	if len(os.Args) < 3 || !utils.StringInSlice(os.Args[1], validCmds) {
		log.Fatalf("Usage: dashboard-manager CMD /path/to/services.yaml\nValid commands: %s", validCmds)
	}
	rootDir := utils.GetProjectRoot()

	data, err := os.ReadFile(os.Args[2])
	if err != nil {log.Fatalf("error reading file: %v", err)}

	var result map[string]interface{}
	if err := yaml.Unmarshal(data, &result); err != nil {log.Fatalf("error reading yaml: %v", err)}

	servicesMap := services.MakeServices(result["services"].(map[string]interface{}))

	// TODO(?) change from manual CLI management to something like Cobra
	if os.Args[1] == "check_services" {
		if len(os.Args) != 3 {log.Fatalf("Usage: dashboard-manager " + os.Args[1] + " /path/to/services.yaml")}
		for _, v := range servicesMap {
			pid, err := v.CheckService()
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Printf("Service '" + v.String() + "' is running with PID " + pid + "\n")
			}
		}
	} else if os.Args[1] == "setup_nginx" {
		if len(os.Args) != 4 {log.Fatalf("Usage: dashboard-manager " + os.Args[1] + " /path/to/services.yaml " +
							    	     "path_to_index.html")}
		nginxStr := services.SetupNginx(servicesMap, rootDir)
		os.WriteFile(os.Args[3], []byte(nginxStr), 0644)
		fmt.Printf("Written nginx setup to '%v'\n", os.Args[3])
		fmt.Printf("Run: 'sudo cp " + os.Args[3] + " /etc/nginx/sites-available/default'\n")
	} else if os.Args[1] == "services_to_html" {
		if len(os.Args) != 4 {log.Fatalf("Usage: dashboard-manager " + os.Args[1] + " /path/to/services.yaml " +
							             "path_to_index.html")}
		outFileSplit := strings.Split(os.Args[3], "/")
		if outFileSplit[len(outFileSplit) - 1] != "index.html" {log.Fatalf("Expected index.html, got " + os.Args[3])}
		baseHtml := rootDir + "/html/base_index.html"
		outHtml := services.ServicesToHtml(servicesMap, baseHtml)
		os.WriteFile(os.Args[3], []byte(outHtml), 0644)
		fmt.Printf("index.html written at '%v'\n", os.Args[3])
	} else if os.Args[1] == "stop_services" {
		if len(os.Args) != 3 {log.Fatalf("Usage: dashboard-manager " + os.Args[1] + " /path/to/services.yaml")}
		res := services.KillAllServices(servicesMap);
		nKilled := 0
		for serviceName, err := range res {
			if err != nil {
				fmt.Printf("%v: %v\n", serviceName, err.Error());
			} else {
				fmt.Printf("%v: killed\n", serviceName)
				nKilled += 1
			}
		}
		fmt.Printf("%v/%v services were killed.\n", nKilled, len(servicesMap))
	} else if os.Args[1] == "start_services" {
		if len(os.Args) != 3 {log.Fatalf("Usage: dashboard-manager " + os.Args[1] + " /path/to/services.yaml")}
		errs, pids := services.StartAllServices(servicesMap);
		nStarted := 0
		for serviceName, err := range errs {
			if err != nil {
				fmt.Printf("%v: %v\n", serviceName, err.Error());
			} else {
				fmt.Printf("%v: started (pid: %v)\n", serviceName, pids[serviceName])
				nStarted += 1
			}
		}
		fmt.Printf("%v/%v services were started.\n", nStarted, len(servicesMap))
	} else if os.Args[1] == "stop_service" {
		if len(os.Args) != 4 {log.Fatalf("Usage: dashboard-manager " + os.Args[1] + " /path/to/services.yaml SERVICE")}
		servicesMap := services.MakeServices(result["services"].(map[string]interface{}))
		serviceToKill := os.Args[3]
		err := services.KillOneService(servicesMap, serviceToKill)
		if err != nil {
			fmt.Printf("%v: %v\n", serviceToKill, err.Error());
		} else {
			fmt.Printf("%v: killed\n", serviceToKill)
		}
	} else if os.Args[1] == "start_service" {
		if len(os.Args) != 4 {log.Fatalf("Usage: dashboard-manager " + os.Args[1] + " /path/to/services.yaml SERVICE")}
		servicesMap := services.MakeServices(result["services"].(map[string]interface{}))
		serviceToStart := os.Args[3]
		pid, err := services.StartOneService(servicesMap[serviceToStart])
		if err != nil {
			fmt.Printf("%v: %v\n", serviceToStart, err.Error());
		} else {
			fmt.Printf("%v: started (pid: %v)\n", serviceToStart, pid)
		}
	}
}
