#!/bin/bash
export FILE_ABS_PATH=$(readlink -f ${BASH_SOURCE[0]})
export CWD=$(dirname $FILE_ABS_PATH)
export SERVICES_PATH=$CWD/"services.yaml"
export EXE_PATH="$CWD/cmd/dashboard-manager"

cd "$CWD"; go build -o "$EXE_PATH" "$CWD"/cmd/dashboard-manager.go; cd -;

if [ $# -eq 1 ]; then
    export SERVICES_PATH=$1
fi

echo "Services path: $SERVICES_PATH"

ls /etc/nginx/sites-available/default || ( echo "/etc/nginx/sites-available/default not found, install nginx"; kill $$ )

$EXE_PATH stop_services $SERVICES_PATH

# generate index.html
$EXE_PATH services_to_html $SERVICES_PATH $CWD/html/index.html

# generate default for nginx
$EXE_PATH setup_nginx $SERVICES_PATH "$CWD"/default
sudo cp "$CWD"/default /etc/nginx/sites-available/default

# restart nginx
echo "Restarting server"
sudo systemctl restart nginx

# startup all services
$EXE_PATH start_services $SERVICES_PATH
