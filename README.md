# Raspberry PI dashboard

Usage: Multiple independent web-apps started at different ports. We'd like one dashboard to access them all.
For this, we need to set up a reverse-proxy. This is akin to docker-compose, but without docker because we like our
services to run natively and we don't need a solution for a problem we don't have (i.e. dependency management).

This code was initially a bunch of python and bash scripts, but it was rewritten to be a standalone golang program with
various CLI commands.

Tested only on Raspbian OS and Ubuntu 22.04.

# Components
 - http server: `nginx`
 - services script: `services.yaml`
 - nginx setup: `./cmd/dashboard-manager.go setup_nginx services.yaml` + `sudo cp default /etc/nginx/sites-available/default`
 - dashboard landing page: `./cmd/dashboard-manager.go services_to_html services.yaml html/index.html` -> writes `html/index.html`
 - startup services (root): `./cmd/dashboard-manager.go start_services services.yaml` -> start all services
 - killall serrvices (root): `./cmd/dashboard-manager.go stop_services services.yaml` -> stop (kill) all services
 - all steps stitched in a single script: `bash run_all.sh services.yaml`

Lowlevel setup: [here](https://gist.github.com/Meehai/5712674f23d678f27d3d2c92c249efd0)

To run at startup, add this to /etc/rc.local: `sudo -u your_user bash /path/to/rpi-dashboard/run_all.sh`

# Requirements

```bash
sudo apt install nginx golang
```

# Services file

See the [test services.yaml](test_services/test_services.yaml) file here.

Format:

```yaml
services:
    name: binary_to_run /path/to/start PORT
    pokedex: bash /home/mihai/code/my-app/startup.sh 4001
    twitter-api: node /home/mihai/code/twitter-api/server.js 4003
```

# Running the tests

Simply run:
```bash
go test ./..
```

# TODOs
- check individual service
- not found on node.js on check_services (see twitter api example)
